#include <iostream>
#include <sstream>
#include <vector>
using namespace std;
int main()
{
    string input;
    cout << "Enter the numbers : ";
    getline(cin, input);
    stringstream ss(input);
    vector<int> numbers;
    int num;
    while (ss >> num)
    {
        numbers.push_back(num);
        if (ss.peek() == ',')
        {
            ss.ignore();
        }
    }

    char opr;
    cout << "enter + for adding nmbrs and - sub numbers :";
    cin >> opr;
    int result = 0;
    if (opr == '+')
    {
        for (int num : numbers)
        {
            result += num;
        }
    }
    else if (opr == '-')
    {
        for (int num : numbers)
        {
            result -= num;
        }
    }
    else
    {
        cout << "invalid oprator";
    }

    cout << "result is : " << result << endl;
    return 0;
}